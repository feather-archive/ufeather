/*
 * ufeather -- simple program to extract a feather archive
 *
 * This program is written in ANSI C and does not require anything
 * beyond the C standard library. It can use POSIX, DOS, or Windows
 * system interfaces to create directories and set file attributes.
 *
 * Copyright 2024 Lassi Kortela
 * SPDX-License-Identifier: ISC
 */

#ifndef UNIX
#ifndef WINDOWS
#define UNKNOWN_OS
#endif
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef UNIX
#include <errno.h>
#include <sys/stat.h>
#endif

#ifdef WINDOWS
#include <windows.h>
#endif

#ifndef PACKAGE_NAME
#define PACKAGE_NAME "ufeather"
#endif

#ifndef COMMAND_NAME
#define COMMAND_NAME PACKAGE_NAME
#endif

#ifndef PACKAGE_VERSION
#define PACKAGE_VERSION "0.1.0"
#endif

static FILE *input;
static FILE *output;
static const char *input_path;
static const char *output_path;
static int absolute_paths_encountered;
static unsigned long expected_size;
static unsigned long expected_crc;
static unsigned long file_size;
static unsigned long crc;

static const unsigned long crc_table[] = { 0x00000000, 0x04c11db7, 0x09823b6e,
    0x0d4326d9, 0x130476dc, 0x17c56b6b, 0x1a864db2, 0x1e475005, 0x2608edb8,
    0x22c9f00f, 0x2f8ad6d6, 0x2b4bcb61, 0x350c9b64, 0x31cd86d3, 0x3c8ea00a,
    0x384fbdbd, 0x4c11db70, 0x48d0c6c7, 0x4593e01e, 0x4152fda9, 0x5f15adac,
    0x5bd4b01b, 0x569796c2, 0x52568b75, 0x6a1936c8, 0x6ed82b7f, 0x639b0da6,
    0x675a1011, 0x791d4014, 0x7ddc5da3, 0x709f7b7a, 0x745e66cd, 0x9823b6e0,
    0x9ce2ab57, 0x91a18d8e, 0x95609039, 0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52,
    0x8664e6e5, 0xbe2b5b58, 0xbaea46ef, 0xb7a96036, 0xb3687d81, 0xad2f2d84,
    0xa9ee3033, 0xa4ad16ea, 0xa06c0b5d, 0xd4326d90, 0xd0f37027, 0xddb056fe,
    0xd9714b49, 0xc7361b4c, 0xc3f706fb, 0xceb42022, 0xca753d95, 0xf23a8028,
    0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1, 0xe13ef6f4, 0xe5ffeb43, 0xe8bccd9a,
    0xec7dd02d, 0x34867077, 0x30476dc0, 0x3d044b19, 0x39c556ae, 0x278206ab,
    0x23431b1c, 0x2e003dc5, 0x2ac12072, 0x128e9dcf, 0x164f8078, 0x1b0ca6a1,
    0x1fcdbb16, 0x018aeb13, 0x054bf6a4, 0x0808d07d, 0x0cc9cdca, 0x7897ab07,
    0x7c56b6b0, 0x71159069, 0x75d48dde, 0x6b93dddb, 0x6f52c06c, 0x6211e6b5,
    0x66d0fb02, 0x5e9f46bf, 0x5a5e5b08, 0x571d7dd1, 0x53dc6066, 0x4d9b3063,
    0x495a2dd4, 0x44190b0d, 0x40d816ba, 0xaca5c697, 0xa864db20, 0xa527fdf9,
    0xa1e6e04e, 0xbfa1b04b, 0xbb60adfc, 0xb6238b25, 0xb2e29692, 0x8aad2b2f,
    0x8e6c3698, 0x832f1041, 0x87ee0df6, 0x99a95df3, 0x9d684044, 0x902b669d,
    0x94ea7b2a, 0xe0b41de7, 0xe4750050, 0xe9362689, 0xedf73b3e, 0xf3b06b3b,
    0xf771768c, 0xfa325055, 0xfef34de2, 0xc6bcf05f, 0xc27dede8, 0xcf3ecb31,
    0xcbffd686, 0xd5b88683, 0xd1799b34, 0xdc3abded, 0xd8fba05a, 0x690ce0ee,
    0x6dcdfd59, 0x608edb80, 0x644fc637, 0x7a089632, 0x7ec98b85, 0x738aad5c,
    0x774bb0eb, 0x4f040d56, 0x4bc510e1, 0x46863638, 0x42472b8f, 0x5c007b8a,
    0x58c1663d, 0x558240e4, 0x51435d53, 0x251d3b9e, 0x21dc2629, 0x2c9f00f0,
    0x285e1d47, 0x36194d42, 0x32d850f5, 0x3f9b762c, 0x3b5a6b9b, 0x0315d626,
    0x07d4cb91, 0x0a97ed48, 0x0e56f0ff, 0x1011a0fa, 0x14d0bd4d, 0x19939b94,
    0x1d528623, 0xf12f560e, 0xf5ee4bb9, 0xf8ad6d60, 0xfc6c70d7, 0xe22b20d2,
    0xe6ea3d65, 0xeba91bbc, 0xef68060b, 0xd727bbb6, 0xd3e6a601, 0xdea580d8,
    0xda649d6f, 0xc423cd6a, 0xc0e2d0dd, 0xcda1f604, 0xc960ebb3, 0xbd3e8d7e,
    0xb9ff90c9, 0xb4bcb610, 0xb07daba7, 0xae3afba2, 0xaafbe615, 0xa7b8c0cc,
    0xa379dd7b, 0x9b3660c6, 0x9ff77d71, 0x92b45ba8, 0x9675461f, 0x8832161a,
    0x8cf30bad, 0x81b02d74, 0x857130c3, 0x5d8a9099, 0x594b8d2e, 0x5408abf7,
    0x50c9b640, 0x4e8ee645, 0x4a4ffbf2, 0x470cdd2b, 0x43cdc09c, 0x7b827d21,
    0x7f436096, 0x7200464f, 0x76c15bf8, 0x68860bfd, 0x6c47164a, 0x61043093,
    0x65c52d24, 0x119b4be9, 0x155a565e, 0x18197087, 0x1cd86d30, 0x029f3d35,
    0x065e2082, 0x0b1d065b, 0x0fdc1bec, 0x3793a651, 0x3352bbe6, 0x3e119d3f,
    0x3ad08088, 0x2497d08d, 0x2056cd3a, 0x2d15ebe3, 0x29d4f654, 0xc5a92679,
    0xc1683bce, 0xcc2b1d17, 0xc8ea00a0, 0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb,
    0xdbee767c, 0xe3a1cbc1, 0xe760d676, 0xea23f0af, 0xeee2ed18, 0xf0a5bd1d,
    0xf464a0aa, 0xf9278673, 0xfde69bc4, 0x89b8fd09, 0x8d79e0be, 0x803ac667,
    0x84fbdbd0, 0x9abc8bd5, 0x9e7d9662, 0x933eb0bb, 0x97ffad0c, 0xafb010b1,
    0xab710d06, 0xa6322bdf, 0xa2f33668, 0xbcb4666d, 0xb8757bda, 0xb5365d03,
    0xb1f740b4 };

static void crc_begin(void) { crc = 0; }

static void crc_update(unsigned char ch)
{
    crc = (crc << 8) ^ crc_table[(crc >> 24) ^ ch];
    crc &= 0xffffffff;
}

static void crc_end(size_t n)
{
    for (; n; n >>= 8) {
        crc_update(n & 255);
    }
    crc = ~crc;
    crc &= 0xffffffff;
}

static void error(char *message)
{
    fprintf(stderr, "%s: %s\n", COMMAND_NAME, message);
    exit(EXIT_FAILURE);
}

#ifdef UNKNOWN_OS
static void error_str(const char *message, const char *str)
{
    fprintf(stderr, "%s: %s %s\n", COMMAND_NAME, message, str);
    exit(EXIT_FAILURE);
}
#endif

#ifdef UNIX
static void error_str_errno(const char *message, const char *str)
{
    fprintf(stderr, "%s: %s %s: %s\n", COMMAND_NAME, message, str,
        strerror(errno));
    exit(EXIT_FAILURE);
}
#endif

static void error_str_stdio(const char *message, const char *path);

#ifdef UNIX
static void error_str_stdio(const char *message, const char *path)
{
    error_str_errno(message, path);
}
#endif

#ifdef UNKNOWN_OS
static void error_str_stdio(const char *message, const char *path)
{
    error_str(message, path);
}
#endif

static void error_memory(void) { error("out of memory"); }

static void error_not_feather(void) { error("not a feather archive"); }

static void error_null_byte(void) { error("null byte in input"); }

static void error_syntax(void) { error("syntax error"); }

static void error_trunc(void) { error("truncated data"); }

static void error_limit(void) { error("limit exceeded"); }

static void close_file(FILE *file, const char *path)
{
    if (fclose(file) == EOF) {
        error_str_stdio("cannot close file", path);
    }
}

static void write_byte(int ch)
{
    if (fputc(ch, output) == EOF) {
        error_str_stdio("cannot write to file", output_path);
    }
}

static int read_byte_or_eof(void)
{
    int ch;

    ch = fgetc(input);
    if (ch == EOF) {
        if (ferror(input)) {
            error_str_stdio("cannot read from file", input_path);
        }
    }
    return ch;
}

static int read_byte(void)
{
    int ch;

    ch = read_byte_or_eof();
    if (ch == EOF) {
        error_trunc();
    }
    return ch;
}

static void consume_archive_header(void)
{
    static char buf[128];
    size_t n;
    int ch;

    memset(buf, 0, sizeof(buf));
    n = 0;
    for (;;) {
        ch = read_byte_or_eof();
        if ((ch == '\n') || (ch == EOF)) {
            break;
        }
        if (!ch) {
            error_not_feather();
        }
        if (n >= sizeof(buf) - 1) {
            error_not_feather();
        }
        buf[n++] = ch;
    }
    if (!strstr(buf, "feather-archive")) {
        error_not_feather();
    }
}

static unsigned long parse_ulong(const char *s)
{
    unsigned long value, new_value;
    unsigned int ch;

    if (!s[0]) {
        error_syntax();
    }
    value = 0;
    for (; (ch = *s); s++) {
        if ((ch < '0') || (ch > '9')) {
            error_syntax();
        }
        new_value = (10 * value) + (ch - '0');
        if (new_value < value) {
            error_limit();
        }
        value = new_value;
    }
    return value;
}

static unsigned long read_record_length_or_eof(void)
{
    char buf[16];
    unsigned long len;
    unsigned int digits;
    int ch;

    memset(buf, 0, sizeof(buf));
    digits = 0;
    ch = read_byte_or_eof();
    if (ch == EOF) {
        return -1UL;
    }
    while (ch != ' ') {
        if ((ch < '0') || (ch > '9')) {
            error_syntax();
        }
        if (digits >= sizeof(buf) - 1) {
            error_limit();
        }
        buf[digits++] = ch;
        ch = read_byte();
    }
    len = parse_ulong(buf);
    if (len < digits + 3) {
        error_trunc();
    }
    return len - (digits + 1);
}

static const char *read_keyword_or_eof(unsigned long *out_value_length)
{
    static char buf[128];
    unsigned long len;
    unsigned int n;
    int ch;

    *out_value_length = 0;
    if ((len = read_record_length_or_eof()) == -1UL) {
        return 0;
    }
    memset(buf, 0, sizeof(buf));
    n = 0;
    for (;;) {
        ch = read_byte();
        if (!ch) {
            error_syntax();
        }
        if (ch == '=') {
            break;
        }
        if (n == len) {
            error_trunc();
        }
        if (n == sizeof(buf) - 1) {
            error_limit();
        }
        buf[n++] = ch;
    }
    *out_value_length = len - n - 2;
    return buf;
}

static void skip_newline(void)
{
    if (read_byte() != '\n') {
        error_syntax();
    }
}

static void skip_value(unsigned long len)
{
    for (; len; len--) {
        read_byte();
    }
    skip_newline();
}

static void output_value(unsigned long len)
{
    int ch;

    if (len < 2) {
        error_trunc();
    }
    len -= 2;
    skip_newline();
    skip_newline();
    file_size += len;
    for (; len; len--) {
        ch = read_byte();
        crc_update(ch);
        write_byte(ch);
    }
    skip_newline();
}

static char *read_string_value(unsigned long len)
{
    char *buf;
    size_t i, n;
    int ch;

    if (len > 65535) {
        error_limit();
    }
    n = len;
    buf = calloc(1, n + 1);
    if (!buf) {
        error_memory();
    }
    for (i = 0; i < n; i++) {
        ch = read_byte();
        if (!ch) {
            error_null_byte();
        }
        buf[i] = ch;
    }
    skip_newline();
    return buf;
}

static unsigned long read_ulong_value(unsigned long len)
{
    unsigned long value;
    char *s;

    s = read_string_value(len);
    value = parse_ulong(s);
    free(s);
    return value;
}

static void ensure_directory_exists(const char *path);

#ifdef UNIX
static void ensure_directory_exists(const char *path)
{
    if (mkdir(path, 0777) == -1) {
        if ((errno != EEXIST) && (errno != EISDIR)) {
            error_str_errno("cannot create directory", path);
        }
    }
}
#endif

#ifdef WINDOWS
#endif

#ifdef UNKNOWN_OS
static void ensure_directory_exists(const char *path)
{
    (void)path;
    error("archive contains subdirectories but not built to make them");
}
#endif

static void end_output_file(void)
{
    if (output) {
        close_file(output, output_path);
        crc_end(file_size);
        if ((expected_size != -1UL) && (file_size != expected_size)) {
            fprintf(stderr,
                "warning: incorrect file size - expected %lu bytes, got %lu "
                "bytes\n",
                expected_size, file_size);
        }
        if ((expected_crc != -1UL) && (crc != expected_crc)) {
            fprintf(stderr,
                "warning: incorrect CRC - expected 0x%08lx, got 0x%08lx\n",
                expected_crc, crc);
        }
    }
    output = 0;
    output_path = 0;
    file_size = 0;
    expected_size = -1UL;
    expected_crc = -1UL;
    crc_begin();
}

static int nondot(const char *s)
{
    for (; *s; s++) {
        if (*s != '.')
            return 1;
    }
    return 0;
}

static void assert_valid_component(const char *s)
{
    if (!nondot(s)) {
        error("bad pathname component");
    }
}

static void begin_output_file(char *path)
{
    char *a;
    char *b;

    end_output_file();
    if (path[0] == '/') {
        if (!absolute_paths_encountered) {
            fprintf(stderr, "ignoring leading / in pathnames");
        }
        absolute_paths_encountered = 1;
    }
    while (path[0] == '/') {
        path++;
    }
    if (strchr(path, '\\')) {
        error("backslash");
    }
    for (a = path; (b = strchr(a, '/')); a = b + 1) {
        *b = 0;
        assert_valid_component(a);
        ensure_directory_exists(path);
        *b = '/';
    }
    assert_valid_component(a);
    output = fopen(path, "wb");
    if (!output) {
        error_str_stdio("cannot open output file", path);
    }
    output_path = path;
}

static void continue_output_file(const char *keyword, unsigned long value_len)
{
    if (!strcmp(keyword, "data")) {
        output_value(value_len);
    } else if (!strcmp(keyword, "size")) {
        if (expected_size != -1UL) {
            error("duplicate");
        }
        expected_size = read_ulong_value(value_len);
    } else if (!strcmp(keyword, "cksum")) {
        if (expected_crc != -1UL) {
            error("duplicate");
        }
        expected_crc = read_ulong_value(value_len);
    } else if (!strcmp(keyword, "uname")) {
        skip_value(value_len);
    } else if (!strcmp(keyword, "gname")) {
        skip_value(value_len);
    } else {
        skip_value(value_len);
    }
}

static int consume_record(void)
{
    const char *keyword;
    unsigned long value_len;

    if (!(keyword = read_keyword_or_eof(&value_len))) {
        return 0;
    }
    if (!strcmp(keyword, "path")) {
        begin_output_file(read_string_value(value_len));
    } else if (output) {
        continue_output_file(keyword, value_len);
    } else {
        printf("ignoring global header: %s\n", keyword);
        skip_value(value_len);
    }
    return 1;
}

static void extract_archive(const char *path)
{
    input = fopen(path, "rb");
    if (!input) {
        error_str_stdio("cannot open input file", path);
    }
    input_path = path;
    consume_archive_header();
    while (consume_record()) { }
    end_output_file();
    close_file(input, input_path);
    input = 0;
    input_path = 0;
}

static const char package_os[] =
#ifdef UNIX
    "Unix"
#endif
#ifdef WINDOWS
    "Windows"
#endif
#ifdef UNKNOWN_OS
    "unknown OS"
#endif
    ;

static void version(void)
{
    printf("%s (%s) %s\n", COMMAND_NAME, PACKAGE_NAME, PACKAGE_VERSION);
    printf("Built for %s\n", package_os);
    exit(EXIT_SUCCESS);
}

static void usage_to(FILE *file, int status)
{
    fprintf(file, "usage: %s archive-file\n", COMMAND_NAME);
    exit(status);
}

static void usage(void) { usage_to(stderr, EXIT_FAILURE); }

int main(int argc, char **argv)
{
    const char *arg;

    if (argc != 2) {
        usage();
    }
    arg = argv[1];
    if (!strcmp(arg, "-h") || !strcmp(arg, "--help")) {
        usage_to(stdout, EXIT_SUCCESS);
    }
    if (!strcmp(arg, "-V") || !strcmp(arg, "--version")) {
        version();
    }
    if (arg[0] == '-') {
        usage();
    }
    extract_archive(arg);
    return EXIT_SUCCESS;
}
